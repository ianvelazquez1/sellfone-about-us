/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-about-us.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-about-us></sellfone-about-us>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
