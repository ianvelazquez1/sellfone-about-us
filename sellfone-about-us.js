import { html, LitElement } from 'lit-element';
import style from './sellfone-about-us-styles.js';

class SellfoneAboutUs extends LitElement {
  static get properties() {
    return {
     imageFirstRow:{
       type:String
     },
     imageSecondRow:{
      	type:String
     },
     titleFirstRow:{
       type:String
     },
     textFirstRow:{
       type:String
     },
     textSecondRow:{
      type:String
     },
     titleSecondRow:{
       type:String
     }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.titleFirstRow="Que hacemos en Sellfone"
    this.textFirstRow="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    this.titleSecondRow="Garantía Sellfone"
    this.imageSecondRow="/images/logo.jpg"
    this.textSecondRow="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."+
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  }

  render() {
    return html`
       <div class="row1">
        <img class="logo" src="${this.logo}" alt="Logo should be here">
        <div class="descriptionContainer">
           <p class="title">${this.titleFirstRow}</p>
           <p class="text">${this.textFirstRow}</p>
        </div>
       </div>
       <div class="row2">
        <div class="descriptionContainer2">
          <div class="subcontainer">
             <div class="section1">
             <p class="title">
              ${this.titleSecondRow}
              </p>
              <img class="subContainerImage" src="${this.imageSecondRow}" alt="">
             </div>
              <p class="text">
            ${this.textSecondRow}
              </p>
          </div>
         
        </div>
       </div>
      `;
    }
}

window.customElements.define("sellfone-about-us", SellfoneAboutUs);
